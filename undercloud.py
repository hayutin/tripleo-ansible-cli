#!/usr/bin/python
#!/usr/bin/env python
# Copyright 2019 Red Hat, Inc.
# All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.


# import the python-tripleoclient
# undercloud cli

from tripleoclient.v1 import undercloud

import sys
import json
import os
import shlex

# See the following for details
# https://opendev.org/openstack/python-tripleoclient/src/branch/
# master/tripleoclient/v1/undercloud.py

DOCUMENTATION = '''
---
module: tripleo_ansible_cli_undercloud
short_description: undercloud install
version_added: ".1"
author: "Wes Hayutin @weshay"
description:
   - Trigger an undercloud install directly from ansible
options:
   force_stack_update:
     description:
        - Do a virtual update of the ephemeral heat stack
     type: bool
     default: 'False'
   no_validations:
     description:
        - Do not perform undercloud configuration validations
     default: 'False'
   inflight-validations:
     description:
        - Activate in-flight validations during the deploy.
     type: bool
     default: 'False'
   dry_run:
     description:
        - run the command with out the real install of the undercloud
     type: bool
     default: 'False'
   force_stack_validation:
    description:
        - enable validations
    type: bool
    default: 'False'
'''

# setup the osc command


class Arg:
    verbose_level = 4


# instantiate the
u = undercloud.InstallUndercloud('tripleo', Arg())

# prog_name = 'openstack undercloud install'
tripleo_args = u.get_parser('openstack undercloud install')

# read the argument string from the arguments file
args_file = sys.argv[1]
args_data = file(args_file).read()

# For this module, we're going to do key=value style arguments.
arguments = shlex.split(args_data)
for arg in arguments:

    # ignore any arguments without an equals in it
    if "=" in arg:

        (key, value) = arg.split("=")

        # if setting the time, the key 'time'
        # will contain the value we want to set the time to

        if key == "dry_run":
            if value == "True":
                tripleo_args.dry_run = True
            else:
                tripleo_args.dry_run = False

        tripleo_args.force_stack_validations = False
        tripleo_args.no_validations = True
        tripleo_args.force_stack_update = False
        tripleo_args.inflight = False

        # execute the install via python-tripleoclient
        rc = u.take_action(tripleo_args)

        if rc != 0:
            print(json.dumps({
                "failed": True,
                "msg": "failed tripleo undercloud install"
            }))
            sys.exit(1)

        print(json.dumps({
            "changed": True,
            "msg": "SUCCESS"
        }))
        sys.exit(0)
